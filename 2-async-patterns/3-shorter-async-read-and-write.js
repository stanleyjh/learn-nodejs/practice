//// This is a better way to write asynchronous function since it is easier to read.

const { readFile, writeFile } = require("fs").promises;

//// An option to use util.promisify() which takes an input for a funciton and returns them as a promise.
// const util = require("util");
// const readFilePromisify = util.promisify(readFile);
// const writeFilePromisify = util.promisify(writeFile);

const start = async () => {
  try {
    // Waiting for the promises to resolve.
    const first = await readFile("./content/first.txt", "utf8");
    const second = await readFile("./content/second.txt", "utf8");
    console.log(first, second);
    // writeText(PATH_TO_NEW_FILE, DATA_TO_BE_PASSED_IN)
    await writeFile("./content/results.txt", `${first} ...... ${second}`);
  } catch (error) {
    console.log(error);
  }
};

start();
