const { readFile, writeFile } = require("fs");

// An asynchronous function using Promise.
const getText = (path) => {
  return new Promise((resolve, reject) => {
    readFile(path, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
};

// An asynchronous function using Promise.
const writeText = (path, data) => {
  return new Promise((reject) => {
    writeFile(path, data, (error) => {
      if (error) {
        reject(error);
      }
    });
  });
};

const start = async () => {
  try {
    // Waiting for the promises to resolve.
    const first = await getText("./content/first.txt");
    const second = await getText("./content/second.txt");

    // writeText(PATH_TO_NEW_FILE, DATA_TO_BE_PASSED_IN)
    writeText("./content/results.txt", `${first} .... ${second}`);
  } catch (error) {
    console.log(error);
  }
};

start();

// getText("./content/first.txt")
//   .then((result) => console.log(result))
//   .catch((error) => console.log(error));
