const http = require("http");

const port = 5000;

const server = http.createServer((request, response) => {
  if (request.url === "/") {
    response.end("<h1>Homepage</h1>");
  } else if (request.url === "/about") {
    for (let i = 0; i < 100; i++) {
      for (let j = 0; j < 1000; j++) {
        console.log(`${i} ${j}`);
      }
    }
    response.end("<h1>About page</h1>");
  } else if (request.url === "/history") {
    response.end("<h1>History</h1>");
  } else {
    response.end(
      "<p>Oops, this page was not found.</p><p>Click <a href='/'>here</a> to navigate back to the homepage.</p>"
    );
  }
});

server.listen(port, () => {
  console.log(`Server started on http://localhost:${port}`);
});
