// Asynchronous reading and writing files.
const { readFile, writeFile } = require("fs");

console.log("start");

// readFile("PATH", "DECODING", callback_function).
readFile("./content/first.txt", "utf8", (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  const first = result;
  readFile("./content/second.txt", "utf8", (err, result) => {
    if (err) {
      console.log(err);
      return;
    }
    const second = result;
    // writeFile("PATH", "DATA", callback_function) function writes data to a specified file.
    writeFile(
      "./content/results.txt",
      `ASYNC: ${first} ... ${second}`,
      (error, result) => {
        if (error) {
          console.log(error);
          return;
        }
        console.log("done with this task");
      }
    );
  });
});
console.log("starting next task");
