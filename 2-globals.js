// GLOBALS

/*
__dirname - path to current directory
__filename - file name
require - a function to use modules
module - reusable code
process - information about the environment where the program is being executed.
*/

console.log(__dirname);
console.log(__filename);
