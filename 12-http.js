const http = require("http");

// An arbitrary number chosen for server port.
const port = 5000;

// create a web server and use a callback function.
const server = http.createServer((req, res) => {
  // For a single request, there should only be one corresponding response.
  if (req.url === "/") {
    res.end("homepage");
  } else if (req.url === "/about") {
    res.end("about");
  } else {
    res.end(
      "<h1>Oops the page is not found</h1> <a href='/'>go to homepage</a>"
    );
  }

  //// Using a switch statement instead.
  // switch (req.url) {
  //   case "/":
  //     res.end("Welcome to our homepage");
  //     break;
  //   case "/about":
  //     res.end("The about page.");
  //     break;
  //   default:
  //     res.end(`
  //       <h1>Oops</h1>
  //       <p>The page cannot be found.</p>
  //       <a href="/">Return to homepage.</a>
  //     `);
  // }

  // Example of a simple server response.
  // res.write("Welcome to our homepage.");
  // res.end();
});

// The server is listening for a client to access localhost:port.
server.listen(port, () => {
  console.log(`The application has started. Go to http://localhost:${port}`);
});
