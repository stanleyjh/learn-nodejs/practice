const path = require("path");

// Shows the delimiter for a file path.
console.log(path.sep);

// the path.join() joins folder and file names together to create a filepath. You can add extra forward slashes and it will be removed.
const filePath = path.join("/content//", "subfolder", "test.txt");
console.log(filePath); // OUTPUT: \content\subfolder\test.txt

// path.basename() method retrieves the base file.
const base = path.basename(filePath);
console.log(base);

// path.resolve() method returns a sequence of paths or path segemetns into an absolute path.
const absolute = path.resolve(__dirname, "content", "subfolder", "test.txt");
console.log(absolute);
