// This creates a web server. When a request is received, a response is sent from reading the ./content/big.txt and sending the response to the requestor as one single file.

const http = require("http");
const fs = require("fs");

const port = 5000;

http
  .createServer((request, response) => {
    const text = fs.readFileSync("./content/big.txt", "utf8");
    response.end(text);
  })
  .listen(port, () => {
    `Server started. Please navigate to http://localhost:${port}`;
  });
