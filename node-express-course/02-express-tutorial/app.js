//

// modules
const express = require("express");
const app = express();
// load data.js to use the people array.
let { people } = require("./data");

// static assets to show web pages.
app.use(express.static("./methods-public"));
// urlencoded() is a middleware built into express. This parses form data.
app.use(express.urlencoded({ extended: false }));

app.get("/api/people", (req, res) => {
  res.json({ success: true, data: people });
});

// routes
// HTTP POST request
app.post("/login", (req, res) => {
  console.log(req.body);

  const { name } = req.body;

  if (name) {
    return res.status(200).send(`Welcome ${name}`);
  } else {
    return res.status(401).send("Please provide a user name.");
  }
});

const port = 3000;

app.listen(port, () =>
  console.log(`Server started. Go to http://localhost:${port}`)
);
