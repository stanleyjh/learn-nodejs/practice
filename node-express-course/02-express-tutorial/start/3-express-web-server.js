// This web server will return an array of two objects when visiting the homepage. Navigating to the /data page will show the products array from data.js.

const { products } = require("../data.js");

console.log(products);

const express = require("express");
const app = express();

const PORT = 3000;

app.get("/", (request, response) => {
  response.json([{ name: "john" }, { name: "susan" }]);
});

app.get("/data", (request, response) => {
  response.json(products);
});

app.listen(PORT, () => {
  console.log(`Server started. Go to http://localhost:${PORT}`);
});
