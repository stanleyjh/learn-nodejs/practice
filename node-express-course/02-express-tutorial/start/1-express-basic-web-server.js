// Most basic web server using express.js.

const express = require("express");
// To use the express library, we create an instance of the express library by invoking express() and it returns an object.
const app = express();

const port = 5000;

// The server is the listening for a get request. Then the callback function is invoked every time a request is sent to root.
app.get("/", (request, response) => {
  console.log("user hit the resource");
  response.send("Home Page");
});

app.get("/about", function (request, response) {
  console.log("user hit the about page.");
  // Express is good at determining the status code. But we can explicity state it.
  response.status(200).send("About Page");
});

// all(PATH, CB_function) method
app.all("*", function (request, response) {
  response
    .status(404)
    .send(
      `<h1>Resource not found.</h1><p>Click <a href='http://localhost:${port}'>here</a> to navigate to the home page.</p>`
    );
});

app.listen(port, () =>
  console.log(`Server started. Go to http://localhost:${port}`)
);
// app.get
// app.post
// app.put
// app.delete
// app.all - handles all types of HTTP methods.
// app.use - responsible for middleware.
// app.listen
