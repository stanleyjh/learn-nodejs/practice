/* 
This web server has a homepage and another page /api/v1/query. 
When you go to http://localhost:3000/api/v1/query?search=a&limit=2, it will return any name starting with "a" and limit the return to 2 entries. This is from the products array in data.js.
*/
const express = require("express");
const app = express();

const { products } = require("../data");

const port = 3000;

app.get("/", (req, res) => {
  res.send("<h1>homepage</h1>");
});

// https://stackabuse.com/get-query-strings-and-parameters-in-express-js/
app.get("/api/v1/query", (req, res) => {
  //// logs what the req.query parameters are if any was passed in the request.
  console.log(req.query);

  // arbitrary properties which are destructured.
  const { search, limit } = req.query;

  // Create a copy of the products array.
  let sortedProducts = [...products];

  // filter sortedProducts if search is defined.
  if (search) {
    sortedProducts = sortedProducts.filter((product) => {
      return product.name.startsWith(search);
    });
  }

  // filter sortedProducts if limit is defined.
  if (limit) {
    // Number() function converts a string to a number.
    sortedProducts = sortedProducts.slice(0, Number(limit));
  }

  // This is an exception handler to catch if there are no sorted products after filtering.
  if (sortedProducts.length < 1) {
    //// Example 1: a simple return saying there are no products based on the query parameter.
    // res.status(200).send("<h2>No products match your search.</h2>");

    //// Example 2: This one is normal and it returns a json object.
    return res.status(200).json({ success: true, data: [] });
  }
  return res.status(200).json(sortedProducts);
});

app.listen(port, () =>
  console.log(`Server started. Go to http://localhost:${port}`)
);
