/*
This file focuses on creating middleware.
Process: request => middleware => response.

Here we create a reqData function that is an example of a middleware to log metadata about requests received.
*/

const express = require("express");
const app = express();

const port = 3000;

// A function which logs request method metadata. This is useful for seeing requests being sent. Express implicitly passes (req, res, next) into the function.
const reqDataLog = (req, res, next) => {
  const method = req.method;
  const url = req.url;
  const date = new Date().toLocaleString();

  console.log(
    `REQUEST INFO: Method: ${method} | URL: ${url} | Timestamp: ${date}`
  );

  // For middleware, you must pass a response, or invoke next() for the next middleware.
  next();
};

// reqDataLog is passed into app.get. This is an example of middleware. The arguments (req, res, next) are implicitly passed into the middleware.
app.get("/", reqDataLog, (req, res) => {
  res.send("home page");
});

app.get("/about", reqDataLog, (req, res) => {
  res.send("about page");
});

app.listen(port, () => console.log(`Server started listening on port ${port}`));
