/* This server shows the use of multiple middleware. 

Navigating to http://localhost:5000?user=john will return an authorized page. If you do not supply the query, you will get an unauthorized page. You will need to supply the query when visiting the about page as well.

*/
const express = require("express");
const app = express();

const port = 5000;

// middleware.
const logger = require("../logger");
// authorize checks if there is a query. If there is no query, it will return an unauthorized page.
const authorize = require("../authorize");

// Place more than one middleware into an array.
app.use([logger, authorize]);

app.get("/", (req, res) => {
  res.send("query");
});

app.get("/about", (req, res) => {
  res.send("about");
});
app.listen(port, () => console.log(`Server is now listening on port ${port} `));
