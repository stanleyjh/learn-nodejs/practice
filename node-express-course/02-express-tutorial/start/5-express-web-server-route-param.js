// This web server has a home page, an /api/products page, an /api/products/:productID route paramater page, and a 404 page.

const express = require("express");
const app = express();
const data = require("../data.js");
const products = data.products;
const port = 3000;

// HomePage
app.get("/", (req, res) => {
  res.send("<h1>Home Page</h1>");
});

// /api/products - returns all products without the description.
app.get("/api/products", (req, res) => {
  // Initialize newProducts with the products array.
  const newProducts = products.map((product) => {
    // destructure - we only want the id, name, and image.
    const { id, name, image } = product;
    // return an object.
    return { id, name, image };
  });
  // Send the newProducts as a response.
  res.json(newProducts);
});

// /api/products/:productID - :PARAMETER_NAME is a route parameter.
// This allows you to navigate to any single product by indicating the product ID number. When a request is received, a req.params object is created and has a value of { productID: 'X' }. X being the ID, which will be a string.
app.get("/api/products/:productID", (req, res) => {
  // console.log(req);
  // console.log(req.params);
  const singleProduct = products.find((item) => {
    return item.id === parseInt(req.params.productID);
  });

  // if the productID does not exist, return 404.
  if (!singleProduct) {
    return res.status(404).send("Sorry, this product does not exist.");
  }
  return res.json(singleProduct);
});

//// The below is an example showing that route parameters can get complicated.
// app.get("/api/products/:productID/reviews/:reviewID", (req, res) => {
//   console.log(req.params);
//   res.send("hello");
// });

// 404 for the home directory
app.get("*", (req, res) => {
  res.send("<h1>Sorry, this page was not found.</h1>");
});

app.listen(port, () => {
  console.log(`Server started. Go to http://localhost:${port}`);
});
