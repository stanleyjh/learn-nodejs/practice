const express = require("express");
const path = require("path");
const app = express();
const port = 5000;

/* 
setup static files and middleware.
- app.use() method is used to setup middleware.
- We created a public directory and placed all static content into the directory. When a request comes in, the static content is served.
*/
app.use(express.static("./public"));

//// This one is actually not needed since you can reference all static files in app.use().
// app.get("/", (request, response) => {
//   // path.resolve() method resolves a sequence of paths or path segments into an absolute path.
//   response.sendFile(path.resolve(__dirname, "./navbar-app/index.html"));
// });

// The all() method routes all HTTP requests.
app.all("*", (request, response) => {
  response.status(404).send("<h1>Sorry this page was not found.</h1>");
});

app.listen(port, function () {
  console.log(`Server started. Go to http://localhost:${port}`);
});
