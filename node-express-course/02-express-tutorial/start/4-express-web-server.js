// This web server has a home page and an /api/products page.

const express = require("express");
const app = express();
const { products } = require("../data.js");

const port = 3000;

// home page
app.get("/", (req, res) => {
  console.log(req.url);
  res.send("<h1>Home Page</h1><a href='/api/products'>products</a>");
});

// /api/products - returns all products without the description.
app.get("/api/products", (req, res) => {
  // Initialize newProducts with the products array.
  const newProducts = products.map((product) => {
    // destructure - we only want the id, name, and image.
    const { id, name, image } = product;
    // return an object.
    return { id, name, image };
  });
  // Send the newProducts as a response.
  res.json(newProducts);
});

// any other page (404)
app.get("*", (req, res) => {
  res.send(
    "<h1>Sorry, this page was not found. Click <a href='/'>here</a> to go back to the homepage.</h1>"
  );
});

app.listen(port, () =>
  console.log(`Server started. Go to http://localhost:${port}`)
);
