// This shows an example of an HTTP get request and returning data from data.js.

// modules
const express = require("express");
const app = express();
let { people } = require("../data");

console.log(people);

// routes
// HTTP get request.
app.get("/api/people", (req, res) => {
  res.status(200).json({ success: true, data: people });
});

const port = 3000;

app.listen(port, () =>
  console.log(`Server started. Go to http://localhost:${port}`)
);
