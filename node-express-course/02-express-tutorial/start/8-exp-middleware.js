/*
This file focuses on creating middleware.
Process: request => middleware => response.

Here we create a reqData function that is an example of a middleware to log metadata about requests received.
*/

const express = require("express");
const app = express();
const logger = require("./logger");

const port = 3000;

// use(PATH, callback) method is used to mount middleware. This is so you don't have to specify the middleware in each route method. This goes before the route methods. If you exclude the path, it will apply to all route methods.
app.use("/api", logger);

// route methods
app.get("/", (req, res) => {
  res.send("home page");
});

app.get("/about", (req, res) => {
  res.send("about page");
});

app.get("/api/products", (req, res) => {
  res.send("<h1>products</h1>");
});

app.get("/api/items", (req, res) => {
  res.send("<h1>Items</h1>");
});

app.listen(port, () => console.log(`Server started listening on port ${port}`));
