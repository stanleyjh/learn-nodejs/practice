/* This references a third party package called Morgan. Morgan is an HTTP request logger middleware for node.js. 

Navigating to http://localhost:5000?user=john will return an authorized page. If you do not supply the query, you will get an unauthorized page. You will need to supply the query when visiting the about page as well.
*/

//
const express = require("express");
const app = express();
const path = require("path");

// 3rd party package to load after installing the module.
const morgan = require("morgan");

const port = 5000;

// middleware.
const logger = require("../logger");
// authorize checks if there is a query. If there is no query, it will return an unauthorized page.
const authorize = require("../authorize");

//// Place more than one middleware into an array.
// app.use([logger, authorize]);

// When we visit any of the sites, data is logged to the console. OUTPUT EXAMPLE: "GET / 200 9 - 2.411 ms"
app.use(morgan("tiny"));

// routes
app.get("/", (req, res) => {
  console.log(path.resolve(__dirname, "./public/index.html"));
  res.send("homepage!");
});

app.get("/about", (req, res) => {
  res.send("about");
});

app.get("/api/items", (req, res) => {
  console.log(req.user);
  res.send("items");
});

app.listen(port, () => console.log(`Server is now listening on port ${port} `));
