// This is a middleware. This checks if there is a query with { user : "john" }.

const authorize = (req, res, next) => {
  const { user } = req.query;

  if (user === "john") {
    req.user = { name: "john", id: 3 };
    next();
    console.log("authorized");
  } else {
    res.status(401).send("Unauthorized");
  }
};

module.exports = authorize;
