// This is a middleware to be used in other files. This middleware console log's an HTTP request's metadata. This is useful for seeing requests being sent. Express implicitly passes (req, res, next) into the function. A third-party npm package that does this is called morgan.
const reqDataLog = (req, res, next) => {
  const method = req.method;
  const url = req.url;
  const timestamp = new Date().toLocaleString();

  console.log(
    `REQUEST INFO: Method: ${method} | URL: ${url} | Timestamp: ${timestamp}`
  );

  // For middleware, you must pass a response, or invoke next() for the next middleware.
  next();
};

module.exports = reqDataLog;
