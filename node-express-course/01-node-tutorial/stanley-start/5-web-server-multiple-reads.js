// This is a web server which returns HTML, CSS, JS, and .svg files when a request is made to the site.

const { readFileSync } = require("fs");
const http = require("http");

const port = 3000;

// get all files
const homepage = readFileSync("./navbar-app/index.html", "utf8");
const homeStyles = readFileSync("./navbar-app/styles.css", "utf8");
const homeLogic = readFileSync("./navbar-app/browser-app.js", "utf8");
const homeLogo = readFileSync("./navbar-app/logo.svg", "utf8");

// Since websites consist of more than an HTML page, a request is sent to this server for each piece of content.
const server = http
  .createServer((request, response) => {
    const url = request.url;
    // Lists every individual request.url.
    console.log(url);
    switch (url) {
      // homepage
      case "/":
        // for content-types, google "mime types".
        response.writeHead(200, { "content-type": "text/html" });
        response.write(homepage);
        response.end();
        break;
      // about
      case "/about":
        response.writeHead(200, { "content-type": "text/html" });
        response.write("<h1>About</h1>");
        response.end();
        break;
      // style
      case "/styles.css":
        response.writeHead(200, { "content-type": "text/css" });
        response.write(homeStyles);
        response.end();
        break;
      // image/logo
      case "/logo.svg":
        response.writeHead(200, { "content-type": "image/svg+xml" });
        response.write(homeLogo);
        response.end();
        break;
      // javascript
      case "/browser-app.js":
        response.writeHead(200, { "content-type": "text/javascript" });
        response.write(homeLogic);
        response.end();
        break;
      // 404
      default:
        response.writeHead(404, { "content-type": "text/html" });
        response.write(
          `<h1>Sorry this page was not found.</h1> <p>Click <a href="http://localhost:${port}">here</a> to navigate to the homepage.</p>`
        );
        response.end();
        break;
    }
  })
  .listen(3000, () => {
    console.log(`Server started. Go to http://localhost:${port}`);
  });
