const { log } = require("console");
const { readFileSync } = require("fs");
const http = require("http");

const port = 3000;

// get files
const homepage = readFileSync("./index.html", "utf8");

const server = http
  .createServer((request, response) => {
    switch (request.url) {
      case "/":
        response.writeHead(200, { "content-type": "text/plain" });
        response.write(homepage);
        response.end();
        break;
      case "/about":
        response.writeHead(200, { "content-type": "text/html" });
        response.write("<h1>About</h1>");
        response.end();
        break;
      default:
        response.writeHead(404, { "content-type": "text/html" });
        response.write("<h1>Sorry this page was not found.</h1>");
        response.end();
        break;
    }
  })
  .listen(3000, () => {
    console.log(`Server started. Go to http://localhost:${port}`);
  });
