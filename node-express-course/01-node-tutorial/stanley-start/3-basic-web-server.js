// Web server which you can access the homepage("/"), about ("/about") or if it isn't found it will return a not found page.

const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
  switch (request.url) {
    // homepage
    case "/":
      console.log(request.url);
      response.writeHead(200, { "content-type": "text/html" });
      response.write("<h1>Home Page</h1>");
      response.end();
      break;
    // about page
    case "/about":
      console.log(request.url);
      response.writeHead(200, { "content-type": "text/html" });
      response.write("<h1>About</h1>");
      response.end();
      break;
    // page not found
    default:
      console.log(request.url);
      response.writeHead(404, { "content-type": "text/html" });
      response.write("<h1>Sorry this page is not found.</h1>");
      response.end();
      break;
  }
});

server.listen(port, () => {
  console.log(`Server started. Go to http://localhost:${port}`);
});
