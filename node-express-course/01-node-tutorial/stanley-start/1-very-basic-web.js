// very basic web server.

const http = require("http");

const port = 3000;

// createServer() method is used to create the server and every time a request is initiated on http://localhost:port, the callback function is invoked.
const server = http
  .createServer((request, response) => {
    console.log("user hit the server");
    // response.end() method must be called on each response. Signaling to the server that all response headers and body have been sent.
    response.end();
  })
  // the .listen() method starts the HTTP server listenting for connections.
  .listen(port, () => {
    console.log(`Server started. Go to http://localhost:${port}`);
  });
