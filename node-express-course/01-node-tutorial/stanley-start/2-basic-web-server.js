// This web server includes the writeHead() method to indicate the response status code as well as the content-type.

const http = require("http");

const port = 3000;

const server = http
  .createServer((request, response) => {
    // writeHead() method writes header information.
    console.log(request.statusCode);
    response.writeHead(200, { "content-type": "text/html" });
    response.write("<h1>Home Page</h1>");
    response.end();
  })
  .listen(port, () => {
    console.log(`Server started. Go to http://localhost:${port}`);
  });
