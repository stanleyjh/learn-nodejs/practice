// Synchronous reading and writing files

const { readFileSync, writeFileSync } = require("fs");

console.log("start");

// readFileSync("FILE_PATH", "ENCODING") function reads the file.
const first = readFileSync("./content/first.txt", "utf8");
const second = readFileSync("./content/second.txt", "utf8");

console.log(first, second);

// writeFileSync("PATH", "DATA", "OPTIONS") function
const third = writeFileSync(
  "./content/results.txt",
  `Hello, here is the result of the two files: ${first} .... ${second}`,
  "utf8"
);

console.log(readFileSync("./content/results.txt", "utf8"));

console.log("done with this task");
console.log("starting the next one");
