const http = require("http");
const fs = require("fs");

const port = 3000;

// server is created.
const server = http.createServer((req, res) => {
  // reads the file.
  const fileStream = fs.createReadStream("../content/big.txt", "utf8");
  fileStream.on("open", () => {
    // pipe() method pushes the readStream into a writeStream.
    fileStream.pipe(res);
  });
  fileStream.on("error", (error) => {
    res.end(error);
  });
});

// listens on a specific port.
server.listen(port, () => {
  console.log(`server started. go to http://localhost:${port}`);
});
