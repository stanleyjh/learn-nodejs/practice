const EventEmitter = require("events");
// create an instance of the EventEmitter class.
const customEmitter = new EventEmitter();

// on - listen for event
// emit - emit an event

customEmitter.on("response", (name, age) => {
  console.log(`Hi, I'm ${name} and I am ${age} years old.`);
});

customEmitter.on("response", () => {
  console.log("data there");
});

// emit an event passing in two arguments for the event listener.
customEmitter.emit("response", "John", 40);
