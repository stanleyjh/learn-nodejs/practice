const EventEmitter = require("events");
// create an instance of the EventEmitter class.
const customEmitter = new EventEmitter();

// on - listen for event
// emit - emit an event

customEmitter.on("response", () => {
  console.log("first response");
});

customEmitter.on("response", () => {
  console.log("data there");
});

customEmitter.emit("response");
