// streams - objects that let you read data from a source or write data to a destination in continious fashion.

// module
const { error } = require("console");
const { createReadStream } = require("fs");

// reads a file using createReadStream() method.
const stream = createReadStream("./content/big.txt", {
  encoding: "utf8",
});

// Event listener which listens for data being emitted.
stream.on("data", (result) => {
  console.log(result);
});

// Event listener listening for error and emits the error.
stream.on("error", (error) => {
  console.log(error);
});
